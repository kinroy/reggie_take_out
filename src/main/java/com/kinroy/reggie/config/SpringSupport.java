package com.kinroy.reggie.config;

import com.kinroy.reggie.common.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@Configuration
@Slf4j
public class SpringSupport extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
        log.info("静态资源映射已开启！");
    }

    /*我们在修改数据时，前端传过来的id会丢失精度，所有在后端获取相应id时进行update会在数据库中找不到数据，
    * 解决的办法就是在传给前端数据的时候，先把id 的long类型转换成String，通过一个转换器来实现这样便可以保证精度
    * 然后，我们在把json对象转成java对象时，也会通过这个转换器，把前端传来的string 类型的id变成原来的long类型
    * */

    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        log.info("转换器已启动");
        MappingJackson2HttpMessageConverter jackson2HttpMessageConverter=new MappingJackson2HttpMessageConverter();
        /*设置我们的对象转换器*/
        jackson2HttpMessageConverter.setObjectMapper(new JacksonObjectMapper());
        converters.add(0,jackson2HttpMessageConverter);
    }
}
