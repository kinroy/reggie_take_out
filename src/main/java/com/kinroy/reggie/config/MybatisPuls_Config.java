package com.kinroy.reggie.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class MybatisPuls_Config {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {


        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        /*添加一个分页过滤器*/

        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        log.info("分页过滤器已开启！");
        return mybatisPlusInterceptor;
    }
}
