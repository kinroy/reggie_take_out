package com.kinroy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.Category;
import com.kinroy.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService cs;

    /*分类管理的分页显示功能*/
    @GetMapping("/page")
    public R<IPage> getbypage(HttpServletRequest request) {
        /*用request从url中获取到分页的信息*/
        String page1 = request.getParameter("page");
        String pageSize1 = request.getParameter("pageSize");
        Long page = Long.valueOf(page1);
        Long pageSize = Long.valueOf(pageSize1);
        /*开始分页的业务逻辑*/
        IPage iPage = new Page(page, pageSize);

        /*实现按sort高低排序*/
        LambdaQueryWrapper<Category> lqw=new LambdaQueryWrapper<>();
        lqw.orderByAsc(Category::getSort);

        IPage page2 = cs.page(iPage,lqw);

        return R.success(page2);

    }

    /*修改功能*/
    @PutMapping
    public R<String> update(@RequestBody Category category) {
        /*前端会传来的数据有id，name，sort，我们就直接拿个一个相应的实体封装一下*/

        /*开始实现修改的业务逻辑*/
        UpdateWrapper<Category> uw = new UpdateWrapper<>();
        uw.lambda().eq(Category::getId, category.getId());
        uw.lambda().set(category.getName() != null, Category::getName, category.getName());
        uw.lambda().set(category.getSort() != null, Category::getSort, category.getSort());

        boolean update = cs.update(uw);
        if (update){
            return R.success("修改成功！");
        }
        return R.error("修改失败，未知错误！");


    }

    /*新增菜品或套餐功能*/
    @PostMapping
    public R<String> add(@RequestBody Category category){
        /*菜品和套餐的区别就是category实体中的 type属性的值不相同，
        * type =1 表示菜品  type=0 表示套餐 ，新增后，前端会发来 name，type，sort属性，
        * 我们就继续用 category 实体 来进行封装
        * */
        boolean save = cs.save(category);
        if (save){
            return R.success("添加成功！");
        }
        return R.error("添加失败！");
    }

    /*删除功能*/
    @DeleteMapping
    public R<String> delete( Long ids){
        /*他妈的，真奇怪啊，为什么delete请求url上带的请求参数可以自动封装到形参上啊，
        * 我tmd的还在纳闷了，写 @DeleteMapping("/{ids}") 然后在 形参上加@PathVariable
        * 妈的死活传不过来，我真的服了
        * */

        /*删除业务的需求还有完善一下，就是当我们的这个套餐或者菜品下有着菜时，我们的业务要求是无法把
        * 这个套餐或者菜品删除的，比如（川菜中存有钵钵鸡、火锅）我们就不能把川菜删除。
        * 其中就涉及到了两个实体 一个是Dish 另一个是 SetMeal  我们要把它们的mapper和service以及service的实现类搞好
        * 才能进行删除功能的完善。
        * */
       log.info("要删除的id是{}",ids);
        boolean b = cs.remove(ids);
        if (b){
            return R.success("删除成功！");
        }
        return R.error("已有关联，删除失败！");
    }

    /*在新增菜品窗口中显示出我们已添加的菜品功能*/
    /*在移动端显示出全部的套餐和菜品*/
    @GetMapping("/list")
    public R<List<Category>> showList(Long type){
        /*移动端要显示所有的套餐和菜品，但是移动端发来的请求不会带请求参数*/
        if (type==null){
            List<Category> list = cs.list();
            return R.success(list);
        }

        LambdaQueryWrapper<Category> lqw=new LambdaQueryWrapper();
        lqw.eq(Category::getType,type);
        List<Category> list = cs.list(lqw);
        return R.success(list);
    }






}
