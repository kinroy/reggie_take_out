package com.kinroy.reggie.controller;

import com.aliyuncs.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.User;
import com.kinroy.reggie.entity.UserDto;
import com.kinroy.reggie.service.UserService;
import com.kinroy.reggie.utils.SMSUtils;
import com.kinroy.reggie.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService us;


    /*获取验证码功能*/
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        /*前端发来的是一个json数据，数据中只有一个属性，那就是phone
         * 我们用一个user实体来封装它
         * */

        //1.获取手机号
        String phone = user.getPhone();

        if (!StringUtils.isEmpty(phone)) {

            //2.生成验证码 调用我们的工具类--> validateCodeUtils
            String code = ValidateCodeUtils.generateValidateCode4String(4);//传4，表示生成4位验证码

            log.info("我们自动生成的验证码为{}",code);

            /*设置一个session，对应的号码，唯一对应着它的验证码*/
            session.setAttribute(phone,code);
            //3.调用阿里云服务来发送短信给这个手机号，里面有我们的验证码 调用我们的工具类--> SMSutils
            /*这里只做了解，因为我没钱去交阿里云的短信服务，所有知道怎么调用api就行了*/
            /*
            SMSUtils.sendMessage("kinroy", "SMS_274900244", phone, code);
            */

        }
        return R.success("验证码发送成功！");


    }


    /*用户登录功能*/
    @PostMapping("/login")
    public R<String> login(@RequestBody UserDto userDto, HttpSession session) {
        /*前端只会传来手机号和验证码，我们只需要从session中获取对应的phone对应的code，
        * 看一下输入的和我们在 sendMsg中存的是否相同，相同返回成功
        */
        log.info("传过来的号码为{}，验证码为{}",userDto.getPhone(),userDto.getCode());
        String code = userDto.getCode();
        String phone = userDto.getPhone();

        String trueCode = (String)session.getAttribute(phone);
        if (code.equals(trueCode)){
            LambdaQueryWrapper<User> lqw=new LambdaQueryWrapper();
            lqw.eq(User::getPhone,phone);
            User one = us.getOne(lqw);
            if (one ==null){
                us.save(userDto);
            }
            session.setAttribute("user",one.getId());
            return R.success("登录成功！");
        }
        return R.error("您输入的验证码有误！");

    }

    /*用户退出功能*/
    @PostMapping("/loginout")
    public R<String> logout(HttpServletRequest request){
        request.getSession().removeAttribute("user");
        return R.success("退出成功！");
    }
}
