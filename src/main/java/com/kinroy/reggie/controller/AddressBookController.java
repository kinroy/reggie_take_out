package com.kinroy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.AddressBook;
import com.kinroy.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/addressBook")
public class AddressBookController {
   @Autowired
   private AddressBookService abs;

   /*获取用户收货地址*/
    @GetMapping("/list")
    public R<List<AddressBook>> getlist(HttpServletRequest request){
        /*登录成功后，我们在移动端的登录逻辑中加上了，登录成功后就在session中加上
        * 登录成功用户的kv， key是user ，value为当前用户登录成功的电话号码
        * */
        HttpSession session = request.getSession();
        Object userId = session.getAttribute("user");
        LambdaQueryWrapper<AddressBook> lqw=new LambdaQueryWrapper<>();
        lqw.eq(AddressBook::getUserId,userId);
        List<AddressBook> address = abs.list(lqw);
        if (address !=null){
            return R.success(address);
        }
        return R.error("您暂未添加任何地址信息！");
    }

    /*修改地址信息的数据回显*/
    @GetMapping("/{addressBookId}")
    public R<AddressBook> show(@PathVariable Long addressBookId){
        log.info("前端传来的地址信息的id为{}",addressBookId);
        AddressBook byId = abs.getById(addressBookId);
        if (byId !=null){
            return R.success(byId);
        }
        return R.error("加载地址信息出错了！");
    }

    /*修改地址信息*/
    @PutMapping
    public R<String> update(@RequestBody AddressBook addressBook){
        log.info("前端传来的地址信息为{}",addressBook.toString());
        boolean b = abs.updateById(addressBook);
        if (b){
            return R.success("修改地址成功！");
        }
        return R.error("修改地址失败！");
    }

    /*修改默认地址*/
    @PutMapping("/default")
    public R<String> updateDefault( @RequestBody AddressBook addressBook){
        Long id = addressBook.getId();
        log.info("封装到entity中前端传来的id为{}",id);
        /*修改默认前，先把所有的地址都设置成非默认，然后把我们业务中想要设置为默认的设置成默认
        * 因此我在addressBookService中自定义了一个业务逻辑
        * */
        boolean b = abs.updateDefault(id);
        if (b){
           return R.success("修改默认地址成功！");
        }
        return R.error("修改默认地址失败！");
    }

    /*添加地址功能*/
    @PostMapping
    public R<String> add(@RequestBody AddressBook addressBook,HttpServletRequest request){
        log.info("前端传来的要添加的地址信息为{}",addressBook.toString());
        /*前端传来的信息中userid是没有值的，我们要自己设置一下*/
        HttpSession session = request.getSession();
        Long userid =(Long) session.getAttribute("user");
        addressBook.setUserId(userid);
        boolean save = abs.save(addressBook);
        if (save){
            R.success("添加地址成功！");
        }
        return null;
    }

    /*删除地址功能*/
    @DeleteMapping
    public  R<String> delete(Long ids){
        log.info("前端传来的要删除的id为{}",ids);
        boolean b = abs.removeById(ids);
        if (b){
            return R.success("地址删除成功！");
        }
        return R.error("地址删除失败！");
    }

    /*下单后获取到默认地址*/
    @GetMapping("/default")
    public R<AddressBook> showDefault(HttpServletRequest request){
        HttpSession session = request.getSession();
        Long userId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<AddressBook> lqw=new LambdaQueryWrapper<>();
        lqw.eq(AddressBook::getUserId,userId);
        lqw.eq(AddressBook::getIsDefault,1);
        AddressBook one = abs.getOne(lqw);

        return R.success(one);
    }


}
