package com.kinroy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.Setmeal;
import com.kinroy.reggie.entity.SetmealDish;
import com.kinroy.reggie.entity.SetmealDto;
import com.kinroy.reggie.service.CategoryService;
import com.kinroy.reggie.service.SetmealDishService;
import com.kinroy.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService ss;
    @Autowired
    private CategoryService cs;
    @Autowired
    private SetmealDishService sds;


    /*分页显示功能(包含条件搜索查询)*/
    @GetMapping("/page")
    public R<IPage> getpage(Long page, Long pageSize, String name) {
        log.info("前端传来的分页信息为：页码{}，页码显示条数{}", page, pageSize);
        IPage<Setmeal> setMealPage = new Page(page, pageSize);
        LambdaQueryWrapper<Setmeal> lqw = new LambdaQueryWrapper<>();
        lqw.like(name != null, Setmeal::getName, name);


        IPage<Setmeal> page1 = ss.page(setMealPage, lqw);
        IPage<SetmealDto> setmealDtoIPage = new Page<>();

        BeanUtils.copyProperties(page1, setmealDtoIPage, "records");

        List<Setmeal> records = page1.getRecords();

        List<SetmealDto> setmealDtoList = records.stream().map((item) -> {
            Long categoryId = item.getCategoryId();
            String name1 = cs.getById(categoryId).getName();
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(item, setmealDto);
            setmealDto.setCategoryName(name1);
            return setmealDto;
        }).collect(Collectors.toList());

        setmealDtoIPage.setRecords(setmealDtoList);
        return R.success(setmealDtoIPage);
    }

    /*添加套餐功能*/
    @PostMapping
    public R<String> add(@RequestBody SetmealDto setmealDto) {
        log.info("前端传来的添加套餐数据为{}", setmealDto);
        ss.addWithSetMealDish(setmealDto);
        return R.success("添加套餐成功！");
    }

    /*编辑功能的数据回显*/
    @GetMapping("/{id}")
    public R<SetmealDto> show(@PathVariable Long id) {
        /*前端传来的是setmal的id*/
        SetmealDto showback = ss.showback(id);
        if (showback != null) {
            return R.success(showback);
        }
        return R.error("无法显示！");
    }

    /*编辑修改功能*/
    @PutMapping
    public R<String> update(@RequestBody SetmealDto setmealDto) {
        ss.upadateWithSetMealDish(setmealDto);
        return R.success("修改成功！");
    }

    /*停售功能（带批量停售）*/
    @PostMapping("status/0")
    public R<String> stopsell(Long[] ids) {
        boolean check = true;
        for (int i = 0; i < ids.length; i++) {
            UpdateWrapper<Setmeal> uw = new UpdateWrapper<>();
            uw.lambda().set(Setmeal::getStatus, 0);
            uw.lambda().eq(Setmeal::getId, ids[i]);
            boolean update = ss.update(uw);
            check = update && check;
        }
        if (check) {
            return R.success("停售操作成功！");
        }
        return R.error("停售操作失败！");
    }

    /*起售功能（带批量起售）*/
    @PostMapping("status/1")
    public R<String> opensell(Long[] ids){
        boolean check = true;
        for (int i = 0; i < ids.length; i++) {
            UpdateWrapper<Setmeal> uw = new UpdateWrapper<>();
            uw.lambda().set(Setmeal::getStatus, 1);
            uw.lambda().eq(Setmeal::getId, ids[i]);
            boolean update = ss.update(uw);
            check = update && check;
        }
        if (check) {
            return R.success("启售操作成功！");
        }
        return R.error("启售操作失败！");
    }

    /*删除功能（带批量删除）*/
    @DeleteMapping
    public R<String> delete(Long[] ids){
        List<Long> Ids=new ArrayList<>();
        for (int i = 0; i < ids.length; i++) {
           Ids.add(ids[i]);
        }
        boolean b = ss.removeByIds(Ids);
        if (b){
            return R.success("删除操作成功！");
        }
        return R.error("删除操作失败！");
    }

    /*移动端的套餐显示功能*/
    @GetMapping("/list")
    public R<List<Setmeal>> showlist(Long categoryId,Long status){
        LambdaQueryWrapper<Setmeal> lqw=new LambdaQueryWrapper<>();
        lqw.eq(Setmeal::getCategoryId,categoryId);
        lqw.eq(Setmeal::getStatus,status);
        List<Setmeal> SetMealList = ss.list(lqw);

        return R.success(SetMealList);
    }

    /*移动端查看套餐内的菜品的功能---->要查的是setMeal——dish表了*/
    @GetMapping("/dish/{setMealId}")
    public R<List<SetmealDish>> showSetmealDish(@PathVariable Long setMealId){
        log.info("获取到的setMealId为{}",setMealId);
        LambdaQueryWrapper<SetmealDish> lqw=new LambdaQueryWrapper<>();
        lqw.eq(SetmealDish::getSetmealId,setMealId);
        List<SetmealDish> SetMealDishList = sds.list(lqw);

        return R.success(SetMealDishList);
    }


}
