package com.kinroy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kinroy.reggie.common.BaseContxt;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.Orders;
import com.kinroy.reggie.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrdersController {
    @Autowired
    private OrdersService os;

    /*用户下单提交功能*/
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){

        os.submit(orders);

        return R.success("提交订单成功");
    }

    /*用户查看自己的订单功能*/
    @GetMapping("/userPage")
    public R<IPage> getOrderPage(Long page,Long pageSize){
        Page<Orders> page1=new Page<>(page,pageSize);
        LambdaQueryWrapper<Orders> lqw=new LambdaQueryWrapper<>();
        Long getid = BaseContxt.getid();
        lqw.eq(Orders::getUserId,getid);
        Page<Orders> page2 = os.page(page1, lqw);

        return R.success(page2);

    }

    /*电脑端显示出用户的订单信息*/
    @GetMapping("/page")
    public R<IPage> getpage(Long page,Long pageSize){
        Page<Orders> page1=new Page<>(page,pageSize);
        Page<Orders> page2 = os.page(page1);
        return R.success(page2);
    }

    /*电脑端修改订单状态*/
    @PutMapping
    public R<String> editOrder(@RequestBody Orders orders){
        boolean update = os.updateById(orders);
        if (update){
            return R.success("修改订单状态成功！");
        }
        return R.error("修改订单状态失败！");
    }


}
