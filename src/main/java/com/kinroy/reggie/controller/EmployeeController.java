package com.kinroy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.Employee;
import com.kinroy.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

/*controller用到的是restful风格开发*/
@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {
    @Autowired
    private EmployeeService es;

    /*登录功能*/
    @PostMapping("/login")
    public R<Employee> login(@RequestBody Employee employee, HttpServletRequest request) {
        /*登录功能的形参中还有一个httpservletrequest 对象，这个的作用就是把登录成功的用户的用户名
         * 存到request域中，这样在跳转到下一个页面的时候还能得到登录成功的那个用户的用户名
         * */
        /*md5加密密码*/
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper();
        /*添加查询条件*/
        lqw.eq(Employee::getUsername, employee.getUsername());
        if (es.getOne(lqw) != null) {
            /*到这里就是账户是能在数据库查到的，但是没验证密码
             * 下面就开始验证密码
             * */
            lqw.eq(Employee::getPassword, password);
            if (es.getOne(lqw) != null) {
                /*到这个地方则是账号和密码都正确了，就看一下员工是否是禁用状态了*/
                lqw.eq(Employee::getStatus, 1);
                if (es.getOne(lqw) != null) {
                    /*到这里，就是大圆满，账号对密码对，账号没被禁用，返回登录成功结果，并且把
                     * 账号id存到request获取的session中
                     * */
                    Employee one = es.getOne(lqw);
                    request.getSession().setAttribute("employee", one.getId());
                    return R.success(one);

                } else {
                    /*这里就是账号对，密码对，但是账号被禁用了*/
                    return R.error("登录失败！，您输入的账号已被禁用！");
                }
            } else {
                /*到这里是账号对，密码不对*/
                return R.error("登录失败，您输入的密码有误！");
            }
        } else {
            /*到这里则是在数据库中用户名都没有*/
            return R.error("登录失败！您输入的账号有误！");
        }

    }

    /*退出功能*/
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request) {
        /*退出操作我们办两件事：
         * 1.把原来登录成功的存在session中的id给删除掉
         * 2.返回结果,就返回一个String就行
         * */
        request.getSession().removeAttribute("employee");
        return R.success("退出成功！");
    }

    /*新增员工功能*/
    @PostMapping
    public R<String> addemployee(@RequestBody Employee employee, HttpServletRequest request) {
        /*前端页面中没有设置密码，在后端我们为其定义一个初始密码并进行md5加密，
         * 这样密码存在数据库里就不是明文了
         * */
        employee.setPassword(DigestUtils.md5DigestAsHex("1234567890".getBytes()));
        /*设置一下操作时间*/
      /*  employee.setUpdateTime(LocalDateTime.now());
        employee.setCreateTime(LocalDateTime.now());*/

        /*设置一下创建者是谁，因为我们要成功登录后才可以添加，则可以把我们成功登录在session中的id拿出来*/
      /*  employee.setCreateUser((Long) request.getSession().getAttribute("employee"));
        employee.setUpdateUser((Long) request.getSession().getAttribute("employee"));*/


        log.info("添加的员工信息为：{}", employee.toString());
        boolean save = es.save(employee);

        return R.success("添加成功！");


    }

    /*员工信息分页显示功能*/
    @GetMapping("/page")

    /*注意注意：我之前写的是 返回值是R<List<Employee>> ，也就是R中的数据模型的data中存的是我们查询好list集合，
     * 但是，我看了前端的代码，前端要相应的是一个Ipage对象，然后调用page对象的records的属性来显示的，
     * 难怪一直显示不出来
     * */
    public R<IPage> getbypage(HttpServletRequest request) {
        /*分页查询的逻辑是查所有，然后根据page，pagesize，来实现分页，因为RESTful风格，
         * 查询是用get请求，请求参数写在了url中，我们先把page，pageSize，还有条件查的name给获取出来
         * */
        String page = request.getParameter("page");
        String name = request.getParameter("name");
        String pageSize = request.getParameter("pageSize");


        /*下面开始分页查询的业务实现*/

        /*mp中分页的实现逻辑：
         * 1.得要有分页过滤器
         * 2.有iPage对象，里面存着分页的信息，page和pageSize
         * 3.service层调用对应的方法page（）,传入page，pageSize 查询后返回查询结果
         * */

        /*数据要把String转成long，因为传的形参是传long的*/
        IPage iPage = new Page(Long.valueOf(page), Long.valueOf(pageSize));

        /*在这里还有一个业务要实现，就是条件分页查询，我们接收的参数中还有一个name，等会就进行判断，
         * 如果name为null，就进行正常分页，如果name不为null就进行条件分页
         *
         * */
        LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper();
        lqw.eq(name != null, Employee::getName, name);
        if (name == null) {
            IPage page1 = es.page(iPage);
            return R.success(page1);
        } else {
            IPage page2 = es.page(iPage, lqw);
            return R.success(page2);
        }

    }


    /*admin账号管理员工启用和禁用功能*/
    @PutMapping
    public R<String> update(@RequestBody Employee employee, HttpServletRequest request) {
        /*这个修改操作是可以复用的，编辑员工信息和启用、禁用都在这里*/
        /*把员工禁用或启用，其实本质上就是修改这个员工的status属性，
         * status为1是启用，status为2是禁用 ，Restful风格中update操作则是发送的是put请求
         * 同时我们要实现的另一个业务是，只有admin用户才能对其他用户进行禁用启用操作，
         * 我们就可以从session中获取当前用户的id，然后进行判断。
         * */

        /*获取当前用户的id，判断是否为admin*/
        Long nowEmployeeId = (Long) request.getSession().getAttribute("employee");
        log.info("当前被修改权限的用户id为{}", employee.getId());
        log.info("当前要修改的用户的权限为{}", employee.getStatus());
        if (nowEmployeeId == 1) {
            /*employee.setUpdateTime(LocalDateTime.now());
            employee.setUpdateUser(nowEmployeeId);*/
            /*前端在我们点击修改用户状态的时候会发来id 和 status两个属性，我们用一个employee来封装它们，
             * 但是前端传来的id是有一点问题的，会丢精度，我们需要处理一下，才能到数据库去找到相应的数据进行修改
             * 我说怪不得一直修改该不了数据
             * */
            boolean b = es.updateById(employee);
            if (b) {
                return R.success("操作成功！");
            }


        }
        return R.error("未知错误！");
    }

    /*修改员工信息的数据回显*/
    @GetMapping("/{id}")
    public R<Employee> show(@PathVariable Long id){
        /*先要获取被修改的用户的信息*/
        LambdaQueryWrapper<Employee> lqw=new LambdaQueryWrapper<>();
        lqw.eq(Employee::getId,id);
        Employee one = es.getOne(lqw);
        if (one !=null){
           return R.success(one);
        }
        return R.error("未知错误！");
    }




}
