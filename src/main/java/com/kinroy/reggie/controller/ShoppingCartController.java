package com.kinroy.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kinroy.reggie.common.BaseContxt;
import com.kinroy.reggie.common.R;
import com.kinroy.reggie.entity.ShoppingCart;
import com.kinroy.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
@Slf4j
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService ss;

    /*获取购物车功能*/
    @GetMapping("/list")
    public R<List<ShoppingCart>> getlist() {
        Long getid = BaseContxt.getid();
        LambdaQueryWrapper<ShoppingCart> lqw=new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId,getid);
        List<ShoppingCart> list = ss.list(lqw);
        return R.success(list);
    }

    /*把菜品和套餐添加到购物车的功能*/
    @PostMapping("/add")
    public R<String> add(@RequestBody ShoppingCart shoppingCart, HttpServletRequest request) {
        log.info("前端传来的添加购物车的信息为{}", shoppingCart.toString());


        /*购物车表中，userid属性和number属性是设置为不能为空的，所有在这里我们要设置一下*/

        //设置userid
        HttpSession session = request.getSession();
        Long userId = (Long) session.getAttribute("user");
        shoppingCart.setUserId(userId);

        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getDishId, shoppingCart.getDishId());

        //判断一下传来的是套餐还是菜品
        Long dishId = shoppingCart.getDishId();
        if (dishId != null) {
            //添加的是菜品
            lqw.eq(ShoppingCart::getUserId, userId);
            ShoppingCart one = ss.getOne(lqw);
            if (one == null) {
                //还没有添加过，那么数量的初始值为1，加到购物车表里
                shoppingCart.setNumber(1);
                ss.save(shoppingCart);
                return R.success("添加成功！");
            }
            //已经能在购物车表中查出来，那么就是已经加过了，现在再加，就在原来的数量上自增1
            one.setNumber(one.getNumber() + 1);
            ss.updateById(one);
            return R.success("添加成功！");

        } else {
            //添加的是套餐
            lqw.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
            ShoppingCart one = ss.getOne(lqw);
            if (one == null) {
                //还没有添加过，那么数量的初始值为1，加到购物车表里
                shoppingCart.setNumber(1);
                ss.save(shoppingCart);
                return R.success("添加成功！");
            }
            //已经能在购物车表中查出来，那么就是已经加过了，现在再加，就在原来的数量上自增1
            one.setNumber(one.getNumber() + 1);
            ss.updateById(one);
            return R.success("添加成功！");
        }


        /*LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
        lqw.eq(ShoppingCart::getUserId, userId);

        ShoppingCart one = ss.getOne(lqw);
        if (one != null) {
            one.setNumber(one.getNumber() + 1);
            ss.updateById(one);
            return R.success("添加成功！");
        }


        shoppingCart.setNumber(1);
        ss.save(shoppingCart);
        return R.success("添加成功！");*/
    }

    /*把菜品从购物车中减一的功能*/
    @PostMapping("/sub")
    public R<String> sub(@RequestBody ShoppingCart shoppingCart, HttpServletRequest request) {
        /*注意注意，是减一，而不是删除，但是我们判断，如果减一之后的值为0的话，我们就可以把这条购物车记录给删除掉*/
        HttpSession session = request.getSession();
        Long userId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> lqw = new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId, userId);
        //先判断要减一的是菜品还套餐
        if (shoppingCart.getDishId() != null) {
            //走到这里则是菜品
            lqw.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
            ShoppingCart one = ss.getOne(lqw);
            Integer number = one.getNumber();
            if (number - 1 == 0) {
                ss.remove(lqw);
                return R.success("减一成功！");
            }
            one.setNumber(number - 1);
            ss.updateById(one);
            return R.success("减一成功！");
        } else {
            //走到这里的则是套餐
            lqw.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
            ShoppingCart one = ss.getOne(lqw);
            Integer number = one.getNumber();
            if (number - 1 == 0) {
                ss.remove(lqw);
                return R.success("减一成功！");
            }
            one.setNumber(number - 1);
            ss.updateById(one);
            return R.success("减一成功！");
        }

    }

    /*清空购物车功能*/
    @DeleteMapping("/clean")
    public R<String> clean(HttpServletRequest request){
        HttpSession session = request.getSession();
        Long userId = (Long) session.getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> lqw=new LambdaQueryWrapper<>();
        lqw.eq(ShoppingCart::getUserId,userId);
        ss.remove(lqw);
        return R.success("清空购物车成功！");
    }



}
