package com.kinroy.reggie.entity;

import com.kinroy.reggie.entity.Setmeal;
import com.kinroy.reggie.entity.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
