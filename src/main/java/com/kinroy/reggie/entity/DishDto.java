package com.kinroy.reggie.entity;

import com.kinroy.reggie.entity.Dish;
import com.kinroy.reggie.entity.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class DishDto extends Dish {
    /*Dto，全称为Data transfer object，即为数据传输对象，用于应对哪些前后端传输数据的时候，
    * 有时候前端传来的数据中的某些属性后端没有响应的实体去完全封装它，比如我们新增菜品功能中，前端
    * 传来的数据就有一个 flavors 属性，里面存有菜品的偏向，但我们的业务实体dish没有这个属性，这个
    * 时候，我们就需要 一个dto来封装它，这个dto就继承了 Dish，表示在原来的dish属性的基础上又添加了
    * 几个属性，用来应对前端传来的不同数据，进行封装。
    * */
    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
