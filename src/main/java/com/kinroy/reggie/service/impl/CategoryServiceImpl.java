package com.kinroy.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.reggie.entity.Category;
import com.kinroy.reggie.entity.Dish;
import com.kinroy.reggie.entity.Setmeal;
import com.kinroy.reggie.mapper.CategoryMapper;
import com.kinroy.reggie.service.CategoryService;
import com.kinroy.reggie.service.DishService;
import com.kinroy.reggie.service.EmployeeService;
import com.kinroy.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private DishService ds;

    @Autowired
    private SetmealService ss;

    @Override
    public boolean remove(Long id) {
        LambdaQueryWrapper<Dish> lqw = new LambdaQueryWrapper();
        lqw.eq(Dish::getCategoryId, id);
        LambdaQueryWrapper<Setmeal> lqw1 = new LambdaQueryWrapper<>();
        lqw1.eq(Setmeal::getCategoryId, id);
        int one = ds.count(lqw);
        int one1 = ss.count(lqw1);
        if (one != 0 || one1 != 0) {
            return false;
        }else {
           return super.removeById(id);
        }

    }
}
