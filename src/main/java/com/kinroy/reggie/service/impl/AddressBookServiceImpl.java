package com.kinroy.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.reggie.entity.AddressBook;
import com.kinroy.reggie.mapper.AddressBookMapper;
import com.kinroy.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@Slf4j
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {

    @Override
    @Transactional
    public boolean updateDefault( Long id) {
        log.info("前端传来的要修改成默认地址的AddressBook的id为{}",id);
        /*这个业务便是完成默认地址的更改*/
        AddressBook byId = this.getById(id);
        Long userId = byId.getUserId();
        UpdateWrapper<AddressBook> uw=new UpdateWrapper<>();
        uw.lambda().eq(AddressBook::getUserId,userId);
        uw.lambda().set(AddressBook::getIsDefault,0);
        boolean update = this.update(uw);
        if (update){
            UpdateWrapper<AddressBook> uw1=new UpdateWrapper<>();
            uw1.lambda().set(AddressBook::getIsDefault,1);
            uw1.lambda().eq(AddressBook::getId,id);
            boolean update1 = this.update(uw1);
            return true;
        }
        return false;
    }
}
