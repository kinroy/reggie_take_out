package com.kinroy.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.reggie.entity.Dish;
import com.kinroy.reggie.entity.DishFlavor;
import com.kinroy.reggie.mapper.DishFlavorMapper;
import com.kinroy.reggie.service.DishFlavorService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
