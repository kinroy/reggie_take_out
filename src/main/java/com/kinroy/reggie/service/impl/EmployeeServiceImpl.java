package com.kinroy.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.reggie.entity.Employee;
import com.kinroy.reggie.mapper.EmployeeMapper;
import com.kinroy.reggie.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
    /*mybatis-plus简化开发，在这里体现得淋漓尽致，
     * 如果说按照以前，我们在service层的实现类中还要 @autowride 将相应的dao层的接口给自动装配，
     * 在这里 只需要 继承一些 mp给我们提供的 ServiceImpl ，然后把相应的dao层接口，和对应的实体类传入，
     * 像这样  ServiceImpl<EmployeeMapper, Employee>   ，然后再实现一下service层的接口，
     * 这样一个service的实现类就开发完成了！
     * */
}
