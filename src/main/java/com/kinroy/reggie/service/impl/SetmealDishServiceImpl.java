package com.kinroy.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinroy.reggie.entity.SetmealDish;
import com.kinroy.reggie.mapper.SetmealDishMapper;
import com.kinroy.reggie.mapper.SetmealMapper;
import com.kinroy.reggie.service.SetmealDishService;
import org.springframework.stereotype.Service;

@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
