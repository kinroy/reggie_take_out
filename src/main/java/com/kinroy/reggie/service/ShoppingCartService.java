package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
}
