package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {
    /*mp的快捷开发，在service层接口 继承一下  IService
    * 然后把实体类当作泛型传进去，即可实现service层的开发，他帮我们把一些基础业务的抽象
    * 方法给写好了。
    *  */
}
