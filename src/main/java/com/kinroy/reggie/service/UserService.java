package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.User;

public interface UserService extends IService<User> {
}
