package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.Category;

public interface CategoryService extends IService<Category> {
    /*我们的业务逻辑中现在存在一个mp没有帮我写的相对不是很普遍的业务操作，
    * 就是在删除分类管理的菜品或套餐的时候，先确定其下是否包含有菜，如果有则无法删除
    * 这个时候我们就要自己写sql了。
    * */
    boolean remove(Long id);
}
