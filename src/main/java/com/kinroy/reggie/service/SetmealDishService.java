package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
