package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {
    boolean updateDefault(Long id);
}
