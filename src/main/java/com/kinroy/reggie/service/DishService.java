package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.Dish;
import com.kinroy.reggie.entity.DishDto;
import com.kinroy.reggie.entity.DishFlavor;

import java.util.List;

public interface DishService extends IService<Dish> {
    void saveWithDishFlavor(DishDto dishDto);

    DishDto show(Long id);

    void updateWithDishFlavor(DishDto dishDto);

    List<DishDto> listwithDishFlavor(Long categoryId,Long status);
}
