package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
