package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.Orders;

public interface OrdersService extends IService<Orders> {

    /*用户提交订单后我们后端要实现的逻辑*/
    void submit(Orders orders);
}
