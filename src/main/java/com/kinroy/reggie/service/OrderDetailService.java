package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
