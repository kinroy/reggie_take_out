package com.kinroy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinroy.reggie.entity.Setmeal;
import com.kinroy.reggie.entity.SetmealDto;

public interface SetmealService extends IService<Setmeal> {
    void addWithSetMealDish(SetmealDto setmealDto);
    SetmealDto showback(Long id);
    void upadateWithSetMealDish(SetmealDto setmealDto);
}
