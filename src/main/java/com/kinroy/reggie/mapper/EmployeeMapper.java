package com.kinroy.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
    /*mp的dao层简化开发，只需继承一下  BaseMapper，然后传入相应的实体类，
     * 即可让mp自动的帮我们写好一般的业务sql和抽象抽象方法。
     * */
}
