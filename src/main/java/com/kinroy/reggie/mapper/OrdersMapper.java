package com.kinroy.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinroy.reggie.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {

}
