package com.kinroy.reggie;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@Slf4j
/*加上 @ServletCompentScan 才能把哪些过滤器的东西给扫描识别到*/
@ServletComponentScan
/*加上 @EnableTransactionManagement 我们设置的事务才能生效，才能实现同成功同失败*/
@EnableTransactionManagement
public class ReggieTakeOutApplication {

    public static void main(String[] args) {
        log.info("瑞吉外卖项目已开启，瑞吉外卖，送啥都快！");
        SpringApplication.run(ReggieTakeOutApplication.class, args);
    }

}
