package com.kinroy.reggie.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class R<T> {
    /*前后端数据交互传输时的封装对象 R 以前写的时候是把这个result数据模型放在controller层，
    但是，以前是没有写有泛型的，现在写了泛型，大家都都可以用，就把它放到了common包下
    * 这个数据模型是公用的，所以可以传泛型
    * */

    private T data;  //数据
    private Integer code;  //前后端协议，1表示成功，0表示失败
    private String msg;  //操作的描述
    private Map map = new HashMap();  //动态数据（不是很清楚是干什么）

    /*下面还提供了三个方法，以前写result数据模型的时候没有写这些方法，
     * 现在让我们来认识一下它们吧
     * */

    /*第一个方法： success方法，先要能看懂这个方法的结构，
     * public static <T> R<T> success(T object){...} 表示一个静态方法（可通过类名直接调用），
     *  返回的参数是我们传的泛型 ，传进这个方法的参数是 我们定义的泛型数据，然后因为是成功了，就把数据
     * 进行封装成一个R数据模型。
     * */
    public static <T> R<T> success(T object) {
        R<T> r = new R<T>();
        r.data = object;
        r.code = 1;
        return r;
    }


    /*第二个方法： error方法，操作失败后调用这个方法，然后封装成R数据模型并返回，
     * 这个R数据模型中存的信息就是失败的信息。
     * */
    public static <T> R<T> error(String msg) {
        R<T> r = new R<T>();
        r.code = 0;
        r.msg = msg;
        return r;
    }

    /*第三个方法：add方法 ,传参是传kv类型的数据，用来操作动态数据的，
     * 我现在也不是很懂这个(time:2023.3.19)
     * */
    public R<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }


}
