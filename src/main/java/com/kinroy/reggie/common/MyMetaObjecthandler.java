package com.kinroy.reggie.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class MyMetaObjecthandler implements MetaObjectHandler {
    /*这个便是元数据管理器，用他来实现公共字段的填充
     * 公共字段便是像我们表中的updatetime、updateuser之类每一个表都存在的属性，同时，每次操作都需要
     * 填写的属性，我之前想的是大家都要填写所有就是想到aop
     * */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info(metaObject.toString());
        log.info("公共字段自动填充【insert】操作已启用");
        /*下面进行公共字段的数据填充，通过调用metaobject的一个setvalue的方法，里面传参传的就是对应要
         * 填充的参数名和对应的值
         * */
        /*通过baseContext来获取咱们之前在loginFilter中存的数据*/
        Long userId = BaseContxt.getid();
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("createUser",userId);
        metaObject.setValue("updateUser",userId);


    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("公共字段自动填充【update】操作已启用");
        Long userId = BaseContxt.getid();
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("updateUser",userId);
    }
}
