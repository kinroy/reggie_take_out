package com.kinroy.reggie.common;

public class BaseContxt {
    /*这个工具封装类用来封装threadlocal，方便我们在公共数据填充中获取到当前用户的id*/
    private static ThreadLocal<Long> threadLocal=new ThreadLocal<>();

    /*下面方法调用后即可设置当前用户的id*/
    public static void setId(Long id){
        threadLocal.set(id);
    }

    /*下面方法用来获取到当前操作用户的id*/
    public static Long getid(){
        Long id = threadLocal.get();
        return id;
    }

}
