package com.kinroy.reggie.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

@RestControllerAdvice
/*加上这个 @responsebody 注解的作用是我们的这个异常处理去在之后要返回一个R数据模型的json对象，需要
 * 用 @responsebody来声明一下
 * */
@ResponseBody
@Slf4j
public class GlobaExceptionHandler {
    /*这个便是我们的全局异常处理器，如果有异常出现，全部都会来到这里进行处理
     * 不同的异常有不同的异常处理方法，通过@ExceptionHandler来区分不同注解
     * */


    /*处理添加了已经存在了的账号的异常*/
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> doSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException exception) {
        /*异常处理方法中的形参为我们对应处理的异常对象，在这里就是 SQLIntegrityConstraintViolationException*/


        /*显示异常信息*/
        log.error(exception.getMessage());

        /*通过显示了的异常信息我们来写我们相应的异常处理，先明确一点，我们的异常为 Duplicate entry 'zhangsan' for key 'idx_username'
         * 但是，并不是所有的 SQLIntegrityConstraintViolationException.class 的异常都是这种类型的信息，我们要针对当前异常做一个判定
         *
         * */
        if (exception.getMessage().contains("Duplicate entry")) {
            /*就是说，我们的异常信息中的字段里有 Duplicate entry 字段的，就按照下面的方式处理*/

            /*我们先将获取到的异常信息字段进行切割，取出那个重名的username，然后返回给前端显示出来，告诉用户，xxx账号已经存在*/
            String[] s = exception.getMessage().split(" ");
            String[] split = s[2].split("'");
            String username=split[1];
            return R.error("您注册的账号" + username + "已存在，请重新输入！");
        }
        return R.error("系统出错了");
    }

}
